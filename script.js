
function Traveler(name) {
    this.name = name;
    this.food = 1;
    this.isHealthy = true;
}
Traveler.prototype = {
    constructor: Traveler,
    hunt: function () {
        this.food += 2
        return this.food
    },
    eat: function () {
        if (this.food >= 1) {
            this.food -= 1
            return this.food
        }
        else {
            this.isHealthy = false;
        }
    }
}

function Wagon(capacity) {
    this.capacity = capacity;
    this.passengers = []
}

Wagon.prototype = {
    constructor: Wagon,
    getAvailableSeatCount: function () {
        seats = this.capacity - this.passengers.length
        console.log(this.passengers)
        return seats
    },
    join: function (Traveler) {
        if (this.passengers.length === this.capacity) {
            return;
        }
        else {
            this.passengers.push(Traveler)
        }
    },
    shouldQuarantine: function () {
        for (let i = 0; i < this.passengers.length; i++) {
            if (this.passengers[i].isHealthy === false) {
                return true;
            }
        }
    },
    totalFood: function () {
        foodTotal = 0;
        for (let i = 0; i < this.passengers.length; i++) {
            foodTotal += this.passengers[i].food
            return foodTotal
        }
    }
}
// Create a wagon that can hold 2 people
let wagon = new Wagon(2);
// Create three travelers
let henrietta = new Traveler('Henrietta');
let juan = new Traveler('Juan');
let maude = new Traveler('Maude');
console.log(`${wagon.getAvailableSeatCount()} 2`);
wagon.join(henrietta);
console.log(`${wagon.getAvailableSeatCount()}  1`);
wagon.join(juan);
wagon.join(maude); // There isn't room for her!
console.log(`${wagon.getAvailableSeatCount()}  0`);
henrietta.hunt(); // get more food
juan.eat();
juan.eat(); // juan is now hungry (sick)
console.log(`${wagon.shouldQuarantine()} true since juan is sick`);
console.log(`${wagon.totalFood()} 3`);